﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serijalizacija {
  public partial class FormCurrencyAdd : Form {
    public CryptoCurrency? NewCurrency { get; set; }
    public FormCurrencyAdd() {
      InitializeComponent();
    }

        private void btnAdd_Click(object sender, EventArgs e) {
            NewCurrency = new CryptoCurrency(tbCurrencyName.Text, tbCurrencyShortName.Text, decimal.Parse(tbBalance.Text));
        }



    }
}
