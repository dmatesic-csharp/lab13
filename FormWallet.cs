namespace Serijalizacija {
    public partial class FormWallet : Form {
        public Wallet wallet;

        public FormWallet() {
            InitializeComponent();
            wallet = new Wallet();
            wallet.Deserialize();
            OsvjeziPopisKripti();
        }

        private void btnAddCurrency_Click(object sender, EventArgs e) {
            FormCurrencyAdd formCurrencyAdd = new FormCurrencyAdd();
            if (formCurrencyAdd.ShowDialog() == DialogResult.OK) {
                CryptoCurrency? newCurrency = formCurrencyAdd.NewCurrency;
                if (newCurrency != null) {
                    wallet.AddCurrency(newCurrency);
                }
            }
            OsvjeziPopisKripti();
        }

        private void OsvjeziPopisKripti() {
            cbCurrencies.Items.Clear();
            cbCurrencies.Items.AddRange(wallet.GetCurrencies().ToArray());
        }

        private void PrikaziIznos(CryptoCurrency objekt) {
            lblBalance.Text = objekt.BalanceHuman;
        }

        private void cbCurrencies_SelectedIndexChanged(object sender, EventArgs e) {
            PrikaziIznos((CryptoCurrency)cbCurrencies.SelectedItem);
        }
    }
}